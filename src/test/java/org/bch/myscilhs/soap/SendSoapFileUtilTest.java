/**
 * © 2014, Boston Children's Hospital. All Rights Reserved.
 * Developed by the Intelligent Health Lab at Children’s Hospital Informatics Program.
 * For more information, see http://chip.org/ihlab and https://github.com/chb
 */
package org.bch.myscilhs.soap;

import org.apache.axiom.om.OMElement;
import org.bch.myscilhs.TestConstants;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class SendSoapFileUtilTest {

  private final String
      fileServiceUrl = TestConstants.TEST_SETTINGS.I2B2_HOST+TestConstants.TEST_SETTINGS.FILE_SEND_SERVICE;
  private final Logger logger = LoggerFactory.getLogger(SendSoapFileUtilTest.class);

  @Test
  public void testGetFRPayload() {
    try {
      OMElement
          elm =
          SendSoapFileUtil.getFrPayLoad(
              TestConstants.SEND_REQ_SCHEMA + TestConstants.SEND_REQ_HEADER
              + TestConstants.SEND_REQ_MESSAGE_BODY + TestConstants.SEND_REQ_FOOTER);
      logger.debug(elm.toString());
    } catch (Exception e) {
      logger.error("Couldn't parse header!", e);
      assertTrue("Exception parsing request message! ", false);
    }
  }

  @Test
  public void testGetFileSize() {
    String size = SendSoapFileUtil.calcFileSize(TestConstants.TEST_PDO);
    assertNotNull("Size string was null!", size);
    assertNotEquals("Size string was empty", 0, size.length());
    logger.debug("File size: " + size);

  }

  @Test
  public void testGetHash() {
    String hash = SendSoapFileUtil.calcFileHash(TestConstants.TEST_PDO);
    assertNotNull("Hash string was null!", hash);
    assertNotEquals("Hash string was empty", 0, hash.length());
    logger.debug("Hash: " + hash);
  }

  @Test
  public void testSendSOAP() throws Exception {
    String
        response =
        SendSoapFileUtil.sendSOAP(fileServiceUrl,
                                  TestConstants.SEND_REQ_SCHEMA + TestConstants.SEND_REQ_HEADER
                                  + TestConstants.SEND_REQ_MESSAGE_BODY
                                  + TestConstants.SEND_REQ_FOOTER, "TEST_PDO_SOAP.xml",
                                  TestConstants.TEST_PDO);
    logger.debug("Soap Response:" + response);
    //logger.debug("The PDO: " + TestConstants.TEST_PDO);
    Assert.doesNotContain(response, "ERROR", "Error message in response");
    Assert.doesNotContain(response, "Exception", "Exception message in response");
  }

  @Test
  public void testSendPatientSOAP() throws Exception {
    String
        response =
        SendSoapFileUtil.sendSOAP(fileServiceUrl,
                                  TestConstants.SEND_REQ_SCHEMA + TestConstants.SEND_REQ_HEADER
                                  + TestConstants.SEND_PATIENT_REQ_MESSAGE_BODY
                                  + TestConstants.SEND_REQ_FOOTER, "TEST_PATIENT_PDO_SOAP.xml",
                                  TestConstants.TEST_PATIENT_PDO);
    logger.debug("Soap Response:" + response);
    //logger.debug("The PDO: " + TestConstants.TEST_PATIENT_PDO);
    Assert.doesNotContain(response, "ERROR", "Error message in response");
    Assert.doesNotContain(response, "Exception", "Exception message in response");
  }
}
