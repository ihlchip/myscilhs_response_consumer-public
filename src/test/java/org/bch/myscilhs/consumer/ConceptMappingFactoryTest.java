/**
 * © 2014, Boston Children's Hospital. All Rights Reserved.
 * Developed by the Intelligent Health Lab at Children’s Hospital Informatics Program.
 * For more information, see http://chip.org/ihlab and https://github.com/chb
 */
package org.bch.myscilhs.consumer;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class ConceptMappingFactoryTest {

  /**
   * Test an invalid xml ODM file is loaded and that the factory successfully initializes
   * regardless.
   */
  @org.junit.Test
  public void testInvalidOdmFile() throws Exception {

    boolean error = false;
    try {
      ConceptMappingFactory
          factory =
          new ConceptMappingFactory(
              "/home/paul/workspace/myscilhs_response_consumer/src/main/resources/public_i2b2_mapping.csv");
      assertNotNull("The ConceptMappingFactory was not successfully initialized!", factory);
      assertNull("The factory map was created incorrectly!", factory.getFieldToODMMap());
    } catch (Exception e) {
      error = true;
      System.out.println("ConceptMappingFactoryTest#testInvalidOdmFile Failed");
    }

    if (!error) {
      System.out.println("ConceptMappingFactoryTest#testInvalidOdmFile completed successfully");
    }
  }

  /**
   * Test the happy path in which a valid xml ODM file is loaded.
   */
  @org.junit.Test
  public void testValidOdmFile() throws Exception {
    ConceptMappingFactory
        factory =
        new ConceptMappingFactory(
            "/home/paul/workspace/myscilhs_response_consumer/src/main/resources/PAHInboundIVRStudy_2014-11-06_concept-dimension.xml");
    assertNotNull("The ConceptMappingFactory was not successfully initialized!", factory);
    assertNotNull("The factory map was not successfully created!", factory.getFieldToODMMap());
    assertNotEquals("The factory map is empty", 0, factory.getFieldToODMMap().size());
  }
}
