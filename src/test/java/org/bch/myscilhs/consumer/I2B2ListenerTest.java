/**
 * © 2014, Boston Children's Hospital. All Rights Reserved.
 * Developed by the Intelligent Health Lab at Children’s Hospital Informatics Program.
 * For more information, see http://chip.org/ihlab and https://github.com/chb
 */
package org.bch.myscilhs.consumer;

import org.bch.myscilhs.dao.FieldBlacklistDao;
import org.bch.myscilhs.dao.FieldBlacklistDaoStaticImpl;
import org.bch.myscilhs.dao.I2B2IdDao;
import org.bch.myscilhs.dao.I2B2IdDaoFileImpl;
import org.bch.myscilhs.dao.MessageDao;
import org.bch.myscilhs.dao.MessageDaoMockImpl;
import org.junit.Test;

import javax.jms.Message;

public class I2B2ListenerTest {

  private I2B2Listener listener;

  @org.junit.Before
  public void setUp() throws Exception {
    FieldBlacklistDao blistDao = new FieldBlacklistDaoStaticImpl();
    I2B2IdDao i2b2IdDao = new I2B2IdDaoFileImpl("/home/paul/workspace/myscilhs_response_consumer/src/main/resources/public_i2b2_mapping.csv");
    ConceptMappingFactory conceptMapping = new ConceptMappingFactory("/home/paul/workspace/myscilhs_response_consumer/src/main/resources/PAHInboundIVRStudy_2014-11-06_concept-dimension.xml");

    /**
     *
     * @param i2b2Host
     * @param sendService
     * @param publishService
     * @param i2b2Domain
     * @param i2b2User
     * @param i2b2Password
     */
    MessageDao messageDao = new MessageDaoMockImpl();

    listener = new I2B2Listener(messageDao, conceptMapping, i2b2IdDao, blistDao);
  }

  @Test
  public void testValidOnMessage() throws Exception {

    // TODO: need to figure out how to create Mock jms message
//    Message message = new Message();
//    listener.onMessage(message);

  }
}
