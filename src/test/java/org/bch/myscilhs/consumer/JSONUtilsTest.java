/**
 * © 2014, Boston Children's Hospital. All Rights Reserved.
 * Developed by the Intelligent Health Lab at Children’s Hospital Informatics Program.
 * For more information, see http://chip.org/ihlab and https://github.com/chb
 */
package org.bch.myscilhs.consumer;

import org.bch.myscilhs.TestConstants;
import org.bch.myscilhs.pojo.AMQMessageVoxeo;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class JSONUtilsTest {

  /**
   * Test the happy path in which a valid json message is passed to the parse function.
   */
  @Test
  public void testValidRedCapParseRawMessageSimple() throws Exception {

    List<AMQMessageVoxeo> responses = JSONUtils.parseRawMessage(TestConstants.TEST_MESSAGE_AMQ_JSON,
                                                                   new ArrayList<String>());

    assertNotNull("Responses were null!", responses);
    assertNotEquals("Responses were empty!", 0, responses.size());
    // 2 because record # 2 is not consented.
    assertEquals("Responses not correct size!", 1, responses.size());
  }

  /**
   * Test the happy path in which a valid json message is passed to the parse function.
   */
  @Test
  public void testValidParseRawMessage() throws Exception {

    List<AMQMessageVoxeo> responses = JSONUtils.parseRawMessage(TestConstants.TEST_MESSAGE_JSON,
                                                                new ArrayList<String>());

    assertNotNull("Responses were null!", responses);
    assertNotEquals("Responses were empty!", 0, responses.size());
    // 2 because record # 2 is not consented.
    //assertEquals("Responses not correct size!", 2, responses.size());
    // 3 because consent field is ignored release 1
    //assertEquals("Responses not correct size!", 3, responses.size());
    //modified raw messages for testing
    assertEquals("Responses not correct size!", 1, responses.size());
  }


  @Test
  public void testConvertToJSON() throws Exception {

    JSONArray obj = JSONUtils.convertToJSON(TestConstants.TEST_MESSAGE_AMQ_JSON);
    assertNotNull("Null JSONObject was returned!", obj);

  }
}
