/**
 * © 2014, Boston Children's Hospital. All Rights Reserved.
 * Developed by the Intelligent Health Lab at Children’s Hospital Informatics Program.
 * For more information, see http://chip.org/ihlab and https://github.com/chb
 */
package org.bch.myscilhs.dao;

import org.bch.myscilhs.TestConstants;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.Assert.*;

public class I2B2IdDaoRestImplTest {

  Logger logger = LoggerFactory.getLogger(I2B2IdDaoRestImplTest.class);

  //Integration test requires file service for i2b2 id to be up
//  @Test
  public void testCallRest() throws Exception {

    I2B2IdDaoRestImpl dao = new I2B2IdDaoRestImpl(TestConstants.TEST_SETTINGS.PUBLIC_ID_PROTOCOL,
                                                  TestConstants.TEST_SETTINGS.PUBLIC_ID_HOST,
                                                  TestConstants.TEST_SETTINGS.PUBLIC_ID_PORT,
                                                  TestConstants.TEST_SETTINGS.PUBLIC_ID_URL,
                                                  TestConstants.TEST_SETTINGS.PUBLIC_ID_USER,
                                                  TestConstants.TEST_SETTINGS.PUBLIC_ID_PASS);
    logger.debug("PUBLIC_ID_PROTOCOL: "+TestConstants.TEST_SETTINGS.PUBLIC_ID_PROTOCOL);
    logger.debug("PUBLIC_ID_HOST: "+TestConstants.TEST_SETTINGS.PUBLIC_ID_HOST);
    logger.debug("PUBLIC_ID_PORT: "+TestConstants.TEST_SETTINGS.PUBLIC_ID_PORT);
    logger.debug("PUBLIC_ID_URL: "+TestConstants.TEST_SETTINGS.PUBLIC_ID_URL);
    logger.debug("PUBLIC_ID_USER: "+TestConstants.TEST_SETTINGS.PUBLIC_ID_USER);
    logger.debug("PUBLIC_ID_PASS: "+TestConstants.TEST_SETTINGS.PUBLIC_ID_PASS);

    String publicId = "patient_1";

    String privateId = dao.callRest(publicId);
    assertNotNull("ERROR: Private ID is NULL!", privateId);
    assertNotEquals("ERROR: Private ID is EMPTY!",privateId,"");
  }
}
