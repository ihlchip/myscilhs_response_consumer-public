/**
 * © 2014, Boston Children's Hospital. All Rights Reserved.
 * Developed by the Intelligent Health Lab at Children’s Hospital Informatics Program.
 * For more information, see http://chip.org/ihlab and https://github.com/chb
 */
package org.bch.myscilhs.dao;

import org.bch.myscilhs.TestConstants;
import org.bch.myscilhs.consumer.ConceptMappingFactory;
import org.bch.myscilhs.consumer.JSONUtils;
import org.bch.myscilhs.pojo.AMQMessageVoxeo;
import org.bch.myscilhs.soap.SendSoapFileUtil;
import org.bch.myscilhs.soap.SendSoapFileUtilTest;
import org.junit.Before;
import org.junit.Test;
import org.springframework.util.Assert;

import java.util.List;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

public class MessageDaoI2B2ImplTest {

  private MessageDaoI2B2Impl messageDao;

  @Before
  public void setUp() throws Exception {
    messageDao = new MessageDaoI2B2Impl(TestConstants.TEST_SETTINGS.I2B2_HOST,
                                                   TestConstants.TEST_SETTINGS.FILE_SEND_SERVICE,
                                                   TestConstants.TEST_SETTINGS.FILE_PUBLISH_SERVICE,
                                                   TestConstants.TEST_SETTINGS.I2B2_DOMAIN,
                                                   TestConstants.TEST_SETTINGS.I2B2_USER,
                                                   TestConstants.TEST_SETTINGS.I2B2_PASSWORD,
                                                   TestConstants.TEST_SETTINGS.I2B2_SOURCE_SYSTEM,
                                                   TestConstants.TEST_SETTINGS.I2B2_SOURCE_INSTITUTION,
                                                   TestConstants.TEST_SETTINGS.I2B2_PROJECT_ID,
                                                   TestConstants.TEST_SETTINGS.I2B2_FILE_LOCATION);

  }

  @Test
  public void testSaveFileRequest() throws Exception {
    SendSoapFileUtilTest testSetup = new SendSoapFileUtilTest();
    testSetup.testSendSOAP();

    String response = messageDao.sendRequest(TestConstants.TEST_PDO_XML_PUBLISH_REQ);

    System.out.println("Received response: " + response);
    Assert.doesNotContain(response, "ERROR", "Error message in response");
    Assert.doesNotContain(response, "Exception", "Exception message in response");
  }

  @Test
  public void testSavePatientFileRequest() throws Exception {
    SendSoapFileUtilTest testSetup = new SendSoapFileUtilTest();
    testSetup.testSendPatientSOAP();

    String response = messageDao.sendRequest(TestConstants.TEST_PATIENT_PDO_XML_PUBLISH_REQ);

    System.out.println("Received response: " + response);
    Assert.doesNotContain(response, "ERROR", "Error message in response");
    Assert.doesNotContain(response, "Exception", "Exception message in response");
  }

  @Test
  public void testPDOGeneration() throws Exception {

    ConceptMappingFactory
        factory =
        new ConceptMappingFactory(
            "/home/paul/workspace/myscilhs_response_consumer/src/main/resources/PAHInboundIVRStudy_2014-11-06_concept-dimension.xml");
    String i2b2ID = "XRTU8GT9FKIS35DSVB";

    FieldBlacklistDaoStaticImpl dao = new FieldBlacklistDaoStaticImpl();

    List<AMQMessageVoxeo>
        responses =
        JSONUtils.parseRawMessage(TestConstants.TEST_MESSAGE_JSON, dao.fetchBlacklistedFields());

    String pdo = messageDao.convertMessagetoPDOV1(responses.get(0), i2b2ID,
                                                          factory.getFieldToODMMap());

    assertNotNull(pdo);
    assertNotEquals("PDO is empty!", pdo, "");

    System.out.println(pdo);
  }

  @Test
  public void testSaveMessageFacts() throws Exception {
    ConceptMappingFactory
        factory = new ConceptMappingFactory(
        "/home/paul/workspace/myscilhs_response_consumer/src/main/resources/PAHInboundIVRStudy_2014-11-06_concept-dimension.xml");
    String i2b2ID = "1111111111";
    FieldBlacklistDaoStaticImpl dao = new FieldBlacklistDaoStaticImpl();
    List<AMQMessageVoxeo>
        responses =
        JSONUtils.parseRawMessage(TestConstants.TEST_MESSAGE_JSON, dao.fetchBlacklistedFields());

    boolean save = messageDao.saveMessageFacts(responses.get(0), i2b2ID,
                                factory.getFieldToODMMap());
    Assert.isTrue(save, "Could not save message!");
  }

  @Test
  public void testSaveMessagePatient() throws Exception {
    ConceptMappingFactory
        factory = new ConceptMappingFactory(
        "/home/paul/workspace/myscilhs_response_consumer/src/main/resources/PAHInboundIVRStudy_2014-11-06_concept-dimension.xml");
    String i2b2ID = "1111111112";
    FieldBlacklistDaoStaticImpl dao = new FieldBlacklistDaoStaticImpl();
    List<AMQMessageVoxeo>
        responses =
        JSONUtils.parseRawMessage(TestConstants.TEST_MESSAGE_JSON, dao.fetchBlacklistedFields());

    boolean save = messageDao.saveMessagePatient(responses.get(0), i2b2ID);
    Assert.isTrue(save, "Could not save message!");
  }
}
