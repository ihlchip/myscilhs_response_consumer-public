/**
 * © 2014, Boston Children's Hospital. All Rights Reserved.
 * Developed by the Intelligent Health Lab at Children’s Hospital Informatics Program.
 * For more information, see http://chip.org/ihlab and https://github.com/chb
 */
package org.bch.myscilhs.dao;

import org.bch.myscilhs.TestConstants;
import org.bch.myscilhs.consumer.I2B2Listener;
import org.bch.myscilhs.consumer.JSONUtils;
import org.bch.myscilhs.pojo.AMQMessageVoxeo;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

public class I2B2IdDaoFileImplTest {

  // Not used currently
//  @Test
  public void testFetchi2b2Identifiers() throws Exception {

    List<AMQMessageVoxeo> responses = JSONUtils.parseRawMessage(TestConstants.TEST_MESSAGE_JSON,
                                                                new ArrayList<String>());
    String surveyId = "1";
    String surveyVersion = "1";

    assertNotNull("Responses were null!", responses);
    assertNotEquals("Responses were empty!", 0, responses.size());
    // 2 because record # 2 is not consented.
    assertEquals("Responses not correct size!", 2, responses.size());

    I2B2IdDao
        dao =
        new I2B2IdDaoFileImpl(
            "/home/paul/workspace/myscilhs_response_consumer/src/main/resources/public_i2b2_mapping.csv");

    Map<String, String> identifiers = dao.fetchi2b2Identifiers(responses);

    assertNotNull("Identifiers were null!", identifiers);
    assertNotEquals("Identifiers were empty!", 0, identifiers.size());
    // 2 because record # 2 is not consented.
    assertEquals("Identifiers not correct size!", 2, identifiers.size());
  }
}
