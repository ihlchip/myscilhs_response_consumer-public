/*
* Copyright (c) 2006-2009 Massachusetts General Hospital
 * All rights reserved. This program and the accompanying materials
* are made available under the terms of the i2b2 Software License v2.1
 * which accompanies this distribution.
 *
 * Contributors:
 * 		Lori Phillips
 * 	        Jeff Klann
 * 	        Paul Blazejewski
 */

package org.bch.myscilhs.soap;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.impl.builder.StAXOMBuilder;
import org.apache.axiom.soap.SOAP11Constants;
import org.apache.axiom.soap.SOAPEnvelope;
import org.apache.axiom.soap.SOAPFactory;
import org.apache.axis2.Constants;
import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.OperationClient;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.context.MessageContext;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.StringReader;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.mail.util.ByteArrayDataSource;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;

// TODO: See if alternate implementation is available to trim dependencies

public class SendSoapFileUtil {
  static final String
      SEND_FILE_REQ_TEMPLATE =
      "<ns7:request xmlns:ns7=\"http://www.i2b2.org/xsd/hive/msg/1.1/\" xmlns:ns2=\"http://www.i2b2.org/xsd/hive/pdo/1.1/\" xmlns:ns4=\"http://www.i2b2.org/xsd/cell/crc/psm/1.1/\" xmlns:ns3=\"http://www.i2b2.org/xsd/cell/crc/pdo/1.1/\" xmlns:ns9=\"http://www.i2b2.org/xsd/cell/pm/1.1/\" xmlns:ns5=\"http://www.i2b2.org/xsd/cell/crc/loader/1.1/\" xmlns:ns6=\"http://www.i2b2.org/xsd/cell/fr/1.0/\" xmlns:ns10=\"http://www.i2b2.org/xsd/cell/crc/psm/querydefinition/1.1/\" xmlns:ns8=\"http://www.i2b2.org/xsd/hive/msg/result/1.1/\" xmlns:ns11=\"http://www.i2b2.org/xsd/cell/crc/psm/analysisdefinition/1.1/\">\n"
      + "    <message_header>\n"
      + "        <i2b2_version_compatible>1.1</i2b2_version_compatible>\n"
      + "        <hl7_version_compatible>2.4</hl7_version_compatible>\n"
      + "        <sending_application>\n"
      + "            <application_name>Manual</application_name>\n"
      + "            <application_version>1.700</application_version>\n"
      + "        </sending_application>\n"
      + "        <sending_facility>\n"
      + "            <facility_name>i2b2 Hive</facility_name>\n"
      + "        </sending_facility>\n"
      + "        <receiving_application>\n"
      + "            <application_name>Project Management Cell</application_name>\n"
      + "            <application_version>1.700</application_version>\n"
      + "        </receiving_application>\n"
      + "        <receiving_facility>\n"
      + "            <facility_name>i2b2 Hive</facility_name>\n"
      + "        </receiving_facility>\n"
      + "        <datetime_of_message>%s</datetime_of_message>\n"
      + "        <security>\n"
      + "            <domain>%s</domain>\n"
      + "            <username>%s</username>\n"
      + "            <password>%s</password>\n"
      + "        </security>\n"
      + "        <message_control_id>\n"
      + "            <instance_num>0</instance_num>\n"
      + "        </message_control_id>\n"
      + "        <processing_id>\n"
      + "            <processing_id>P</processing_id>\n"
      + "            <processing_mode>I</processing_mode>\n"
      + "        </processing_id>\n"
      + "        <accept_acknowledgement_type>AL</accept_acknowledgement_type>\n"
      + "        <application_acknowledgement_type>AL</application_acknowledgement_type>\n"
      + "        <country_code>US</country_code>\n"
      + "        <project_id>%s</project_id>\n"
      + "    </message_header>\n"
      + "<message_body>\n"
      + "    <ns6:sendfile_request>\n"
      + "        <upload_file size=\"%s\" name=\"%s\" overwrite=\"true\" algorithm=\"MD5\" hash=\"%s\" date=\"%s\"/>\n"
      + "    </ns6:sendfile_request>\n"
      + "</message_body>\n"
      + "</ns7:request>\n";

  public static final String
      SAVE_FILE_REQ_TEMPLATE =
      "<ns7:request xmlns:ns7=\"http://www.i2b2.org/xsd/hive/msg/1.1/\" xmlns:ns2=\"http://www.i2b2.org/xsd/hive/pdo/1.1/\" xmlns:ns4=\"http://www.i2b2.org/xsd/cell/crc/psm/1.1/\" xmlns:ns3=\"http://www.i2b2.org/xsd/cell/crc/pdo/1.1/\" xmlns:ns9=\"http://www.i2b2.org/xsd/cell/pm/1.1/\" xmlns:ns5=\"http://www.i2b2.org/xsd/cell/crc/loader/1.1/\" xmlns:ns6=\"http://www.i2b2.org/xsd/cell/fr/1.0/\" xmlns:ns10=\"http://www.i2b2.org/xsd/cell/crc/psm/querydefinition/1.1/\" xmlns:ns8=\"http://www.i2b2.org/xsd/hive/msg/result/1.1/\" xmlns:ns11=\"http://www.i2b2.org/xsd/cell/crc/psm/analysisdefinition/1.1/\">\n"
      + "    <message_header>\n"
      + "        <i2b2_version_compatible>1.1</i2b2_version_compatible>\n"
      + "        <hl7_version_compatible>2.4</hl7_version_compatible>\n"
      + "        <sending_application>\n"
      + "            <application_name>Manual</application_name>\n"
      + "            <application_version>1.700</application_version>\n"
      + "        </sending_application>\n"
      + "        <sending_facility>\n"
      + "            <facility_name>i2b2 Hive</facility_name>\n"
      + "        </sending_facility>\n"
      + "        <receiving_application>\n"
      + "            <application_name>Project Management Cell</application_name>\n"
      + "            <application_version>1.700</application_version>\n"
      + "        </receiving_application>\n"
      + "        <receiving_facility>\n"
      + "            <facility_name>i2b2 Hive</facility_name>\n"
      + "        </receiving_facility>\n"
      + "        <datetime_of_message>%s</datetime_of_message>"
      + "        <security>\n"
      + "            <domain>%s</domain>\n"
      + "            <username>%s</username>\n"
      + "            <password>%s</password>\n"
      + "        </security>\n"
      + "        <message_control_id>\n"
      + "            <instance_num>0</instance_num>\n"
      + "        </message_control_id>\n"
      + "        <processing_id>\n"
      + "            <processing_id>P</processing_id>\n"
      + "            <processing_mode>I</processing_mode>\n"
      + "        </processing_id>\n"
      + "        <accept_acknowledgement_type>AL</accept_acknowledgement_type>\n"
      + "        <application_acknowledgement_type>AL</application_acknowledgement_type>\n"
      + "        <country_code>US</country_code>\n"
      + "        <project_id>%s</project_id>\n"
      + "    </message_header>\n"
      + "    <message_body>\n"
      + "        <ns5:publish_data_request>\n"
      + "            <input_list>\n"
      + "                <data_file>\n"
      + "                    <location_uri protocol_name=\"LOCAL\">%s</location_uri>\n"
      + "                    <data_format_type>PDO</data_format_type>\n"
      + "                    <source_system_cd>i2b2</source_system_cd>\n"
      + "                    <load_label>%s</load_label>\n"
      + "                </data_file>\n"
      + "            </input_list>\n"
      + "            <load_list clear_temp_load_tables=\"true\" commit_flag=\"true\">\n"
      + "                <load_observation_set append_flag=\"true\" />\n"
      + "                <load_patient_set ignore_bad_data=\"true\" encrypt_blob=\"false\" delete_existing_data=\"false\" />\n"
      + "                <load_pid_set xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"ns5:fact_load_optionType\" append_flag=\"true\" />\n"
      + "                <load_eid_set xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"ns5:fact_load_optionType\" append_flag=\"true\" />\n"
      + "                <load_event_set ignore_bad_data=\"true\" encrypt_blob=\"false\" delete_existing_data=\"false\" />\n"
      + "            </load_list>\n"
      + "            <output_list detail=\"true\">\n"
      + "                <observation_set onlykeys=\"true\" />\n"
      + "                <patient_set onlykeys=\"true\" />\n"
      + "                <pid_set onlykeys=\"true\" />\n"
      + "                <eid_set onlykeys=\"true\" />\n"
      + "            </output_list>\n"
      + "        </ns5:publish_data_request>\n"
      + "    </message_body>\n"
      + "</ns7:request>\n";
  private static Logger logger = LoggerFactory.getLogger(SendSoapFileUtil.class);

  /**
   * Function to convert Ont requestVdo to OMElement
   *
   * @param requestVdo String requestVdo to send to Ont web service
   * @return An OMElement containing the Ont web service requestVdo
   */
  public static OMElement getFrPayLoad(String requestVdo) throws Exception {
    OMElement lineItem;
    try {
      StringReader strReader = new StringReader(requestVdo);
      XMLInputFactory xif = XMLInputFactory.newInstance();
      XMLStreamReader reader = xif.createXMLStreamReader(strReader);

      StAXOMBuilder builder = new StAXOMBuilder(reader);
      lineItem = builder.getDocumentElement();
    } catch (FactoryConfigurationError e) {
      logger.error(e.getMessage());
      throw new Exception(e);
    }
    return lineItem;
  }

  /**
   *
   * @param endpointURL
   * @param request
   * @param filename
   * @param fileData
   * @return
   * @throws Exception
   */
  public static String sendSOAP(String endpointURL, String request, String filename,
                                String fileData) throws Exception {
    OMElement getFr = getFrPayLoad(request);
    Options options = new Options();
    String serviceURL = endpointURL;
    if (serviceURL.endsWith("/")) {
      serviceURL = serviceURL.substring(0, serviceURL.length() - 1);
    }

    logger.debug("FR URL:" + serviceURL);
    options.setTo(new EndpointReference(serviceURL));

    options.setAction("urn:sendfileRequest");
    options.setSoapVersionURI(SOAP11Constants.SOAP_ENVELOPE_NAMESPACE_URI);

    // Increase the time out to receive large attachments
    options.setTimeOutInMilliSeconds(10000);

    options.setProperty(Constants.Configuration.ENABLE_SWA,
                        Constants.VALUE_TRUE);
    options.setProperty(Constants.Configuration.CACHE_ATTACHMENTS,
                        Constants.VALUE_TRUE);
    options.setProperty(Constants.Configuration.ATTACHMENT_TEMP_DIR, "temp");
    options.setProperty(Constants.Configuration.FILE_SIZE_THRESHOLD, "4000");

    ServiceClient sender = FrServiceClient.getServiceClient();
    sender.setOptions(options);

    OperationClient mepClient = sender.createClient(ServiceClient.ANON_OUT_IN_OP);

    MessageContext mc = new MessageContext();

    ByteArrayDataSource bytes = new ByteArrayDataSource(fileData.getBytes(), "");
    javax.activation.DataHandler dataHandler = new javax.activation.DataHandler(bytes);
    mc.addAttachment(filename, dataHandler);
    mc.setDoingSwA(true);

    SOAPFactory sfac = OMAbstractFactory.getSOAP11Factory();
    SOAPEnvelope env = sfac.getDefaultEnvelope();

    env.getBody().addChild(getFr);

    mc.setEnvelope(env);
    mepClient.addMessageContext(mc);
    mepClient.execute(true);

    MessageContext inMsgtCtx = mepClient.getMessageContext("In");
    SOAPEnvelope responseEnv = inMsgtCtx.getEnvelope();

    OMElement soapResponse = responseEnv.getBody().getFirstElement();

    OMElement soapResult = soapResponse.getFirstElement();

    String i2b2Response = soapResponse.toString();

    logger.debug("FR Request URL: " + serviceURL + "\n" + request);

    return soapResponse.toString();
    //mepClient.getMessageContext(WSDLConstants.MESSAGE_LABEL_IN_VALUE).getEnvelope().toStringWithConsume();

  }

  public static String generateFileSaveRequest( String i2b2date, String i2b2Domain, String i2b2User,
                                            String i2b2Pass, String projectId, String fullFilePath,
                                            String fileName) throws Exception {
    return String
        .format(SAVE_FILE_REQ_TEMPLATE, i2b2date, i2b2Domain, i2b2User, i2b2Pass, projectId,
                fullFilePath, fileName);
  }

  public static String calcFileHash(String fileData) {
    MessageDigest md;
    String hash = null;
    try {
      md = MessageDigest.getInstance("MD5");
      hash = new String(Hex.encodeHex(md.digest(fileData.getBytes())));
    } catch (NoSuchAlgorithmException e) {
      logger.error("Error MD5 hashing file data", e);
    }

    return hash;
  }

  public static String calcFileSize(String fileData) {
    if (fileData == null || fileData.isEmpty()) {
      return "0";
    }
    return BigInteger.valueOf(fileData.length()).toString();
  }

  public static String generateFileSendRequest(String date, String i2b2Domain, String i2b2User,
                                               String i2b2Pass, String projectId, String fileSize,
                                               String fileName, String fileHash) {
    return String
        .format(SEND_FILE_REQ_TEMPLATE, date, i2b2Domain, i2b2User, i2b2Pass, projectId, fileSize,
                fileName, fileHash, date);
  }
}
