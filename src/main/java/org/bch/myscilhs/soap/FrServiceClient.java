/*
* Copyright (c) 2006-2009 Massachusetts General Hospital 
 * All rights reserved. This program and the accompanying materials 
* are made available under the terms of the i2b2 Software License v2.1 
 * which accompanies this distribution. 
 * 
 * Contributors:
 * 		Raj Kuttan
 * 		Lori Phillips
 * 	        Paul Blazejewski
 */

package org.bch.myscilhs.soap;

import org.apache.axis2.AxisFault;
import org.apache.axis2.client.ServiceClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class FrServiceClient {

  private static Logger logger = LoggerFactory.getLogger(FrServiceClient.class);

  private static ServiceClient sender = null;

  private FrServiceClient() {
  }

  public static ServiceClient getServiceClient() throws AxisFault {
    if (sender == null) {
      try {
        sender = new ServiceClient();
      } catch (AxisFault e) {
        if (logger.isErrorEnabled()) {
          logger.error("Caught exception creating singleton service!", e);
        }
        throw e;
      }

    }
    return sender;
  }


}
