/**
 * © 2014, Boston Children's Hospital. All Rights Reserved.
 * Developed by the Intelligent Health Lab at Children’s Hospital Informatics Program.
 * For more information, see http://chip.org/ihlab and https://github.com/chb
 */
package org.bch.myscilhs.consumer;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.ActiveMQSslConnectionFactory;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.bch.myscilhs.dao.FieldBlacklistDao;
import org.bch.myscilhs.dao.FieldBlacklistDaoStaticImpl;
import org.bch.myscilhs.dao.I2B2IdDao;
import org.bch.myscilhs.dao.I2B2IdDaoRestImpl;
import org.bch.myscilhs.dao.MessageDao;
import org.bch.myscilhs.dao.MessageDaoI2B2Impl;
import org.bch.myscilhs.pojo.ConsumerSettings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;

/**
 * An ActiveMQ I2B2 Consumer for MySCILHS
 */
@Component
public class ResponseConsumer {
  private static Logger logger = LoggerFactory.getLogger(ResponseConsumer.class);

  @Autowired
  ConsumerSettings consumerSettings;
  @Autowired
  MessageDao messageDao;
  @Autowired
  I2B2IdDao i2b2IdDao;
  @Autowired
  FieldBlacklistDao blacklistDao;

  @Bean
  public ActiveMQConnectionFactory initFactory() throws java.io.IOException, InterruptedException {

    Connection connection = null;
    Session session = null;
    Destination dest;
    MessageConsumer consumer;
    ActiveMQSslConnectionFactory
        factory = null;
    try {
      factory =
          new ActiveMQSslConnectionFactory(
              consumerSettings.MQ_PROTOCOL + "://" + consumerSettings.MQ_HOST + ":" + consumerSettings.MQ_PORT);
      factory.setUserName(consumerSettings.MQ_USER);
      factory.setPassword(consumerSettings.MQ_PASSWORD);
      if (logger.isDebugEnabled()) {
        logger.debug("Using keystore: " + consumerSettings.MQ_KEYSTORE);
      }
      factory.setKeyStore(consumerSettings.MQ_KEYSTORE);
      factory.setKeyStorePassword(consumerSettings.MQ_KEYSTORE_PASSWORD);
      factory.setTrustStore(consumerSettings.MQ_TRUSTSTORE);
      factory.setTrustStorePassword(consumerSettings.MQ_KEYSTORE_PASSWORD);
      connection = factory.createConnection();
      session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
      dest = session.createQueue(consumerSettings.MQ_QUEUE_NAME);
      consumer = session.createConsumer(dest);

      ConceptMappingFactory
          mappingFactory =
          new ConceptMappingFactory(consumerSettings.CONCEPT_FILES_LOCATION);

      if(mappingFactory == null || mappingFactory.getFieldToODMMap() == null || mappingFactory.getFieldToODMMap().isEmpty()){
        throw new Exception("Could not load concept map! I2B2 Loader not started!");
      }

      MessageListener listener = new I2B2Listener(messageDao, mappingFactory, i2b2IdDao, blacklistDao);
      consumer.setMessageListener(listener);
      connection.start();

    } catch (JMSException jmse) {
      if (logger.isErrorEnabled()) {
        logger.error("Exception connecting to queue: ", jmse);
      }
    } catch (Exception e) {
      if (logger.isErrorEnabled()) {
        logger.error("Exception setting up ssl connection.", e);
      }
    }
    if (logger.isDebugEnabled()) {
      logger.debug("Connection setup complete: ");
    }

//    Thread.sleep(1000);
//
//    try {
//      if (session != null) {
//        session.close();
//      }
//      if (connection != null) {
//        connection.close();
//      }
//    } catch (Exception jmse) {
//      if (logger.isErrorEnabled()) {
//        logger.error("Exception closing connection to queue: ", jmse);
//      }
//    }
    return factory;
  }

}

