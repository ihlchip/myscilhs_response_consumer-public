/**
 * © 2014, Boston Children's Hospital. All Rights Reserved.
 * Developed by the Intelligent Health Lab at Children’s Hospital Informatics Program.
 * For more information, see http://chip.org/ihlab and https://github.com/chb
 */
package org.bch.myscilhs.consumer;

import org.bch.myscilhs.dao.FieldBlacklistDao;
import org.bch.myscilhs.dao.I2B2IdDao;
import org.bch.myscilhs.dao.MessageDao;
import org.bch.myscilhs.pojo.AMQMessage;
import org.bch.myscilhs.pojo.AMQMessageVoxeo;
import org.bch.myscilhs.pojo.ODMMapping;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

/**
 * Pulls Voxeo responses off the queue and saves them to i2b2.
 */
public class I2B2Listener implements MessageListener {

  final ConceptMappingFactory conceptMapper;
  final MessageDao messageDao;
  final I2B2IdDao i2b2IdDao;
  final FieldBlacklistDao fieldBlacklistDao;
  /* The underlying implementation is logback which is thread safe.
   * Remove this logger if switching to a non-threadsafe logging implementation.
   */
  private Logger logger = LoggerFactory.getLogger(I2B2Listener.class);
  private Logger auditLogger = LoggerFactory.getLogger("mq_message_audit");

  /**
   *
   * @param messageDao
   * @param conceptMapper
   * @param i2b2IdDao
   * @param fieldBlacklistDao
   */
  public I2B2Listener(MessageDao messageDao, ConceptMappingFactory conceptMapper,
                      I2B2IdDao i2b2IdDao, FieldBlacklistDao fieldBlacklistDao) {
    this.messageDao = messageDao;
    this.conceptMapper = conceptMapper;
    this.i2b2IdDao = i2b2IdDao;
    this.fieldBlacklistDao = fieldBlacklistDao;
  }

  @Override
  public void onMessage(Message message) {
    try {
      String txt = null;
      if (message != null) {
        if (logger.isDebugEnabled()) {
          logger.debug("Message info: " + message);
        }
        txt = ((TextMessage) message).getText();
        if (auditLogger.isDebugEnabled()) {
          auditLogger.debug("Message Id: "+message.getJMSMessageID()+" Received: " + txt);
        }
      }

      List<AMQMessageVoxeo> parsedMessages = null;
      try {
        List<String> blacklist = fieldBlacklistDao.fetchBlacklistedFields();
        parsedMessages = JSONUtils.parseRawMessage(txt, blacklist);
      } catch (Exception e) {
        if (logger.isErrorEnabled()) {
          logger.error("Caught Exception parsing message: ", e);
        }
        if (auditLogger.isErrorEnabled()) {
          auditLogger.error(
              "Caught Exception parsing message with Message Id: : " + message.getJMSMessageID());
        }
      }

      if (parsedMessages != null && !parsedMessages.isEmpty()) {
        if (logger.isDebugEnabled()) {
          logger.debug(" [x] Sending message to i2b2 '" + parsedMessages.size() + "'");
        }

        // Fetch each i2b2 id
        Map<String, String> publicIdToi2b2Id = i2b2IdDao.fetchi2b2Identifiers(parsedMessages);
        Map<String, ODMMapping> conceptMappings = conceptMapper.getFieldToODMMap();

        for (AMQMessage parsedMessage : parsedMessages) {

          boolean sendPatient = false;
          boolean sendMessage = false;

          if(publicIdToi2b2Id != null && !publicIdToi2b2Id.isEmpty() && publicIdToi2b2Id.get(parsedMessage.getSubjectId()) != null){

            //Save the Patient - Does not overwrite
            sendPatient = messageDao
                .saveMessagePatient(parsedMessage, publicIdToi2b2Id.get(parsedMessage.getSubjectId()));

            // Load file into i2b2
            sendMessage = messageDao
                .saveMessageFacts(parsedMessage, publicIdToi2b2Id.get(parsedMessage.getSubjectId()),
                                  conceptMappings);
          }
          if(!sendPatient){
            if (logger.isErrorEnabled()) {
              logger.error(
                  "Saving patient to i2b2 failed for subject id: " + parsedMessage.getSubjectId());
            }
          }
          if(!sendMessage){
            if (logger.isErrorEnabled()) {
              logger.error(
                  "Saving message to i2b2 failed for subject id: " + parsedMessage.getSubjectId());
            }
            if (auditLogger.isErrorEnabled()) {
              auditLogger.error(
                  "Saving message to i2b2 failed for Message Id: : " + message.getJMSMessageID());
            }
          }
        }
      }
      if (logger.isDebugEnabled()) {
        logger.debug(" [x] Done");
      }

    } catch (JMSException jmse) {
      if (logger.isErrorEnabled()) {
        logger.error("Exception pulling from queue: ", jmse);
      }
    }

  }

}
