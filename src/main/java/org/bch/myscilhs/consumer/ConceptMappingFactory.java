/**
 * © 2014, Boston Children's Hospital. All Rights Reserved.
 * Developed by the Intelligent Health Lab at Children’s Hospital Informatics Program.
 * For more information, see http://chip.org/ihlab and https://github.com/chb
 */
package org.bch.myscilhs.consumer;

import org.bch.myscilhs.pojo.ODMMapping;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

/**
 * Reads a ODM XML file for mapping response fields to concept codes for the i2b2 PDO.
 */
public class ConceptMappingFactory {

  private Logger logger = LoggerFactory.getLogger(ConceptMappingFactory.class);
  private Map<String, ODMMapping> fieldToODMMap;

  public ConceptMappingFactory(String odmFile) {
    // TODO: make this into a factory rather than a holder with multiple surveys
    // supported in the map

    fieldToODMMap = loadODMMap(odmFile);
  }

  /**
   * Returns a Map of loaded ODM mappings from the input fileName. This assumes odm_code matches the
   * field name used in the response field.
   *
   * Not static to take advantage of the logger,
   *
   * @param fileName - ODM file to read mappings from.
   * @return a map of odm codes to odm mapping.
   */
  Map<String, ODMMapping> loadODMMap(String fileName) {
    Map<String, ODMMapping> tempODMMap = null;

    try {

      File odmFile = new File(fileName);
      DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
      Document doc = dBuilder.parse(odmFile);

      //optional, but recommended
      //read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
      doc.getDocumentElement().normalize();

      logger.debug("ODM file successfully loaded.");

      NodeList nList = doc.getElementsByTagName("map");
      logger.debug("Mapping " + nList.getLength() + " field mappings.");
      tempODMMap = new HashMap<>(nList.getLength());

      for (int i = 0; i < nList.getLength(); i++) {

        Node nNode = nList.item(i);

        if (nNode.getNodeType() == Node.ELEMENT_NODE) {

          Element eElement = (Element) nNode;

          String odmCode = eElement.getAttribute("odm_code");
          String conceptCode = eElement.getAttribute("concept_code");
          String varName = eElement.getAttribute("varName");
          String type = eElement.getAttribute("type");
          String answer = eElement.getAttribute("answer");

          ODMMapping mapping = new ODMMapping(odmCode, conceptCode, varName, type, answer);
          tempODMMap.put(odmCode, mapping);
        }
      }

      logger.debug(nList.getLength() + " concept maps were created.");
    } catch (Exception e) {
      logger.error("Caught Exception building Concept Mapping from file: " + fileName, e);
    }
    return tempODMMap;
  }

  /**
   * @return a shallow copy of the odm map. The ODMMapping fields are final so this is sufficient
   * for immutability.
   */
  public Map<String, ODMMapping> getFieldToODMMap() {
    if (fieldToODMMap == null) {
      return null;
    }
    return new HashMap<>(fieldToODMMap);
  }
}
