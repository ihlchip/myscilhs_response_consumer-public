/**
 * © 2014, Boston Children's Hospital. All Rights Reserved.
 * Developed by the Intelligent Health Lab at Children’s Hospital Informatics Program.
 * For more information, see http://chip.org/ihlab and https://github.com/chb
 */
package org.bch.myscilhs.consumer;

import org.bch.myscilhs.pojo.AMQMessageVoxeo;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class JSONUtils {

  static final String JSON_FIELD = "field_name";
  static final String JSON_VALUE = "value";
  static final String JSON_ARRAY = "responses";
  static final String RECORD_ID_FIELD = "record";
  static final String SUBJECT_ID_FIELD = "subject_id";
  static final String SURVEY_ID_FIELD = "survey_id";
  static final String VERSION_ID_FIELD = "version_id";
  static final String EVENT_ID_FIELD = "event_id";
  static final String ARM_ID_FIELD = "arm_id";
  static final String CONSENT_FIELD = "consent";

  private static final Logger logger = LoggerFactory.getLogger(JSONUtils.class);

  static public List<AMQMessageVoxeo> parseRawMessage(String messageText, List<String> blacklist) {

    List<AMQMessageVoxeo> parsedResponses = new ArrayList<>();

    //determine # records and add them to list in loop

    Set<String> knownRecords = new HashSet<>();
    Map<String, String> subjectIds = new HashMap<>();
    Map<String, String> surveyIds = new HashMap<>();
    Map<String, String> versionIds = new HashMap<>();
    Map<String, String> eventIds = new HashMap<>();
    Map<String, String> armIds = new HashMap<>();
    Map<String, Boolean> consentMap = new HashMap<>();
    LinkedHashMap<String, LinkedHashMap<String, String>> fieldMaps
        = new LinkedHashMap<>();

    // Assumes JSON is structured into an array of fields, either with an recordId that describe a
    // complete record. This may be unnecessary for Voxeo but RedCap at least has the potential to
    // send multiple records in a single message.
    JSONArray fields = convertToJSON(messageText);

    //logger.debug("Converting to json array of length:" + fields.length());

    for (int i = 0; i < fields.length(); i++) {
      JSONObject field = fields.getJSONObject(i);
      String recordId = field.getString(RECORD_ID_FIELD);
      String subjectId = field.getString(SUBJECT_ID_FIELD);

      if(subjectIds.get(recordId) == null) {
        //logger.debug("Setting subjectId Value:" + subjectId);
        subjectIds.put(recordId, subjectId);
      }
      //logger.debug("Processing Record Id :" + recordId);
      if (!knownRecords.contains(recordId)) {
        //logger.debug("Added to known records Record Id :" + recordId);
        knownRecords.add(recordId);
      }
      //Warning: Switch statement with String is only compatible java 7+
      switch (field.getString(JSON_FIELD)) {
        case SURVEY_ID_FIELD:
          if(surveyIds.get(recordId) == null) {
            surveyIds.put(recordId, field.getString(JSON_VALUE));
          }
          break;
        case VERSION_ID_FIELD:
          if(versionIds.get(recordId) == null) {
            versionIds.put(recordId, field.getString(JSON_VALUE));
          }
          break;
        case EVENT_ID_FIELD:
          if(eventIds.get(recordId) == null) {
            eventIds.put(recordId, field.getString(JSON_VALUE));
          }
          break;
        case ARM_ID_FIELD:
          if(armIds.get(recordId) == null) {
            armIds.put(recordId, field.getString(JSON_VALUE));
          }
          break;
        case CONSENT_FIELD:
          if(consentMap.get(recordId) == null) {
            consentMap.put(recordId, Integer.parseInt(field.getString(JSON_VALUE)) == 1);
          }
          break;
        default:
          if (fieldMaps.get(recordId) == null) {
            fieldMaps.put(recordId, new LinkedHashMap<String, String>());
          }
          //logger.debug("Added to "+field+" for Record Id :" + recordId + " to default case.");
          fieldMaps.get(recordId).put(field.getString(JSON_FIELD), field.getString(JSON_VALUE));
      }

    }

    for (String rId : knownRecords) {
      // Only accept explicitly consented records per MYSCILHS-21
      //if (consentMap.get(rId) != null && consentMap.get(rId).equals(Boolean.TRUE)) {
        AMQMessageVoxeo surveyResults = new AMQMessageVoxeo(rId, subjectIds.get(rId),
                                                            surveyIds.get(rId), versionIds.get(rId),
                                                            eventIds.get(rId), armIds.get(rId));
        LinkedHashMap<String, String> otherFields = fieldMaps.get(rId);
        for (String key : otherFields.keySet()) {
          if (!blacklist.contains(key)) {
            surveyResults.addSurveyField(key, otherFields.get(key));
          }
        }
        parsedResponses.add(surveyResults);
      //}
    }
    return parsedResponses;

  }

  /**
   * Separation of concerns for testing the JSON conversion implementation.
   *
   * @return message converted with to JSON using JSON library
   */
  static JSONArray convertToJSON(String messageText) {
    return new JSONArray(messageText);
  }

}
