/**
 * © 2014, Boston Children's Hospital. All Rights Reserved.
 * Developed by the Intelligent Health Lab at Children’s Hospital Informatics Program.
 * For more information, see http://chip.org/ihlab and https://github.com/chb
 */
package org.bch.myscilhs.spring;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.reloading.FileChangedReloadingStrategy;
import org.apache.commons.configuration.reloading.ReloadingStrategy;
import org.bch.myscilhs.dao.FieldBlacklistDao;
import org.bch.myscilhs.dao.FieldBlacklistDaoStaticImpl;
import org.bch.myscilhs.dao.I2B2IdDao;
import org.bch.myscilhs.dao.I2B2IdDaoRestImpl;
import org.bch.myscilhs.dao.MessageDao;
import org.bch.myscilhs.dao.MessageDaoI2B2Impl;
import org.bch.myscilhs.pojo.ConsumerSettings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jndi.JndiTemplate;

import javax.naming.NamingException;

@Configuration
@ComponentScan(basePackages = {"org.bch.myscilhs.consumer","org.bch.myscilhs.dao","org.bch.myscilhs.spring"})
public class AppConfig {

  private static final String PROP_MQ_NAME = "myscilhs.amq.name";
  private static final String PROP_MQ_PROTOCOL = "myscilhs.amq.protocol";
  private static final String PROP_MQ_HOST = "myscilhs.amq.host";
  private static final String PROP_MQ_PORT = "myscilhs.amq.port";
  private static final String PROP_MQ_USER = "myscilhs.amq.user";
  private static final String PROP_MQ_PASS = "myscilhs.amq.pass";
  private static final String PROP_MQ_KEYSTORE = "myscilhs.amq.keystore";
  private static final String PROP_MQ_TRUSTSTORE = "myscilhs.amq.truststore";
  private static final String PROP_MQ_KEYSTORE_PASS = "myscilhs.amq.keystore.pass";
  private static final String PROP_I2B2_DOMAIN = "myscilhs.i2b2.domain";
  private static final String PROP_I2B2_USER = "myscilhs.i2b2.user";
  private static final String PROP_I2B2_PASS = "myscilhs.i2b2.pass";
  private static final String PROP_I2B2_HOST = "myscilhs.i2b2.host";
  private static final String PROP_I2B2_SOURCE_INST = "myscilhs.i2b2.source.institution";
  private static final String PROP_I2B2_SOURCE_SYS = "myscilhs.i2b2.source.system";
  private static final String PROP_I2B2_PROJECT = "myscilhs.i2b2.projectid";
  private static final String PROP_I2B2_FILE_LOC = "myscilhs.i2b2.file.location";
  private static final String PROP_I2B2_FILE_PUBLISH = "myscilhs.i2b2.file.url.publish";
  private static final String PROP_I2B2_FILE_SEND = "myscilhs.i2b2.file.url.send";
  private static final String PROP_I2B2_CONCEPTS = "myscilhs.i2b2.concept.location";
  private static final String PROP_PUBLIC_PROTOCOL = "myscilhs.publicid.protocol";
  private static final String PROP_PUBLIC_HOST = "myscilhs.publicid.host";
  private static final String PROP_PUBLIC_PORT = "myscilhs.publicid.port";
  private static final String PROP_PUBLIC_SERVICE = "myscilhs.publicid.service";
  private static final String PROP_PUBLIC_USER = "myscilhs.publicid.user";
  private static final String PROP_PUBLIC_PASS = "myscilhs.publicid.pass";

  private final Logger logger = LoggerFactory.getLogger(AppConfig.class);

  @Bean
  public ReloadingStrategy reloadingStrategy() {
    FileChangedReloadingStrategy strategy = new FileChangedReloadingStrategy();
    strategy.setRefreshDelay(1000);
    return strategy;
  }

  @Bean
  public PropertiesConfiguration applicationProperties() {
    try {
      JndiTemplate jndiTemplate = new JndiTemplate();
      String propertiesFile = jndiTemplate.lookup("java:comp/env/myscilhsI2B2LoaderConfigFile", String.class);
      if (propertiesFile == null) {
        throw new RuntimeException("Could not find mySCILHS properties file string from JNDI");
      } else {
        PropertiesConfiguration pc = new PropertiesConfiguration(propertiesFile);
        pc.setReloadingStrategy(reloadingStrategy());
        return pc;
      }
    } catch (ConfigurationException ce) {
      logger.error("Cannot access application properties file. Check server and web application configuration.", ce);
      throw new RuntimeException(ce);
    } catch (NamingException ne) {
      logger.error("Problem doing JNDI lookup of myscilhs configuration path", ne);
      throw new RuntimeException(ne);
    }
  }

  @Bean
  @Autowired
  ConsumerSettings consumerSettings(PropertiesConfiguration applicationProperties){

    //PropertiesConfiguration applicationProperties = applicationProperties();
    ConsumerSettings settings = null;
    try {
        settings =
        new ConsumerSettings(applicationProperties.getString(PROP_MQ_NAME),
                             applicationProperties.getString(PROP_MQ_PROTOCOL),
                             applicationProperties.getString(PROP_MQ_HOST),
                             applicationProperties.getString(PROP_MQ_PORT),
                             applicationProperties.getString(PROP_MQ_USER),
                             applicationProperties.getString(PROP_MQ_PASS),
                             applicationProperties.getString(PROP_MQ_KEYSTORE),
                             applicationProperties.getString(PROP_MQ_TRUSTSTORE),
                             applicationProperties.getString(PROP_MQ_KEYSTORE_PASS),
                             applicationProperties.getString(PROP_I2B2_DOMAIN),
                             applicationProperties.getString(PROP_I2B2_USER),
                             applicationProperties.getString(PROP_I2B2_PASS),
                             applicationProperties.getString(PROP_I2B2_HOST),
                             applicationProperties.getString(PROP_I2B2_SOURCE_INST),
                             applicationProperties.getString(PROP_I2B2_SOURCE_SYS),
                             applicationProperties.getString(PROP_I2B2_PROJECT),
                             applicationProperties.getString(PROP_I2B2_FILE_LOC),
                             applicationProperties.getString(PROP_I2B2_FILE_PUBLISH),
                             applicationProperties.getString(PROP_I2B2_FILE_SEND),
                             applicationProperties.getString(PROP_I2B2_CONCEPTS),
                             applicationProperties.getString(PROP_PUBLIC_PROTOCOL),
                             applicationProperties.getString(PROP_PUBLIC_HOST),
                             applicationProperties.getString(PROP_PUBLIC_PORT),
                             applicationProperties.getString(PROP_PUBLIC_SERVICE),
                             applicationProperties.getString(PROP_PUBLIC_USER),
                             applicationProperties.getString(PROP_PUBLIC_PASS));
    } catch (Exception e) {
      logger.error("Error parsing properties file. Please check all required properties exist!", e);
      throw e;
    }
    return settings;
  }

  @Bean
  @Autowired
  MessageDao messageDao(PropertiesConfiguration applicationProperties){
      MessageDao messageDao = new MessageDaoI2B2Impl(applicationProperties.getString(PROP_I2B2_HOST),
                                                     applicationProperties.getString(PROP_I2B2_FILE_SEND),
                                                     applicationProperties.getString(PROP_I2B2_FILE_PUBLISH),
                                                 applicationProperties.getString(PROP_I2B2_DOMAIN),
                                                 applicationProperties.getString(PROP_I2B2_USER),
                                                 applicationProperties.getString(PROP_I2B2_PASS),
                                                 applicationProperties.getString(PROP_I2B2_SOURCE_SYS),
                                                 applicationProperties.getString(PROP_I2B2_SOURCE_INST),
                                                 applicationProperties.getString(PROP_I2B2_PROJECT),
                                                 applicationProperties.getString(PROP_I2B2_FILE_LOC));
    return messageDao;
  }

  @Bean
  @Autowired
  I2B2IdDao i2b2IdDao(PropertiesConfiguration applicationProperties){
    I2B2IdDao i2b2IdDao = new I2B2IdDaoRestImpl(applicationProperties.getString(PROP_PUBLIC_PROTOCOL),
                                                   applicationProperties.getString(PROP_PUBLIC_HOST),
                                                   applicationProperties.getString(PROP_PUBLIC_PORT),
                                                   applicationProperties.getString(PROP_PUBLIC_SERVICE),
                                                   applicationProperties.getString(PROP_PUBLIC_USER),
                                                   applicationProperties.getString(PROP_PUBLIC_PASS));
    return i2b2IdDao;
  }

  @Bean
  FieldBlacklistDao blacklistDao(){
    return new FieldBlacklistDaoStaticImpl();
  }
}


