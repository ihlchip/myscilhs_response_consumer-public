/**
 * © 2014, Boston Children's Hospital. All Rights Reserved.
 * Developed by the Intelligent Health Lab at Children’s Hospital Informatics Program.
 * For more information, see http://chip.org/ihlab and https://github.com/chb
 */
package org.bch.myscilhs.dao;

import org.bch.myscilhs.pojo.AMQMessageVoxeo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This will be handled by a CSV Uploader service. For test purposes I will load from a file.
 */
public class I2B2IdDaoFileImpl implements I2B2IdDao {

  private final Logger logger = LoggerFactory.getLogger(I2B2IdDaoFileImpl.class);
  private final String fileLocation;

  private I2B2IdDaoFileImpl() {
    fileLocation = null;
  }

  public I2B2IdDaoFileImpl(String fileLocation) {
    this.fileLocation = fileLocation;
  }

  /**
   * This should call a CSV upload service, but for first pass its just reads a file at the
   * specified location.
   */
  public Map<String, String> fetchi2b2Identifiers(List<AMQMessageVoxeo> messages) {
    Map<String, String> publicToi2b2Map = new HashMap<>();
    Map<String, String> messagePublicToi2b2Map = new HashMap<>(messages.size());

    try {
      FileReader mappingFile = new FileReader(fileLocation);
      BufferedReader reader = new BufferedReader(mappingFile);
      String line;

      while ((line = reader.readLine()) != null) {
        String[] ids = line.split(",");
        publicToi2b2Map.put(ids[0], ids[1]);
      }

    } catch (Exception e) {
      if (logger.isErrorEnabled()) {
        logger.error("Could not read File: ", e);
      }
    }

    for (AMQMessageVoxeo message : messages) {
      String publicId = message.getSubjectId();

      if (publicToi2b2Map.get(publicId) != null) {
        messagePublicToi2b2Map.put(publicId, publicToi2b2Map.get(publicId));
      } else {
        if (logger.isErrorEnabled()) {
          logger.error("Could not find i2b2 id for record id: " + message.getRecordId()
                       + " public id: " + publicId);
        }
      }
    }

    return messagePublicToi2b2Map;
  }

}
