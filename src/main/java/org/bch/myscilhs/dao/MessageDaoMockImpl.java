/**
 * © 2014, Boston Children's Hospital. All Rights Reserved.
 * Developed by the Intelligent Health Lab at Children’s Hospital Informatics Program.
 * For more information, see http://chip.org/ihlab and https://github.com/chb
 */
package org.bch.myscilhs.dao;

import org.bch.myscilhs.pojo.AMQMessage;
import org.bch.myscilhs.pojo.ODMMapping;

import java.util.Map;

public class MessageDaoMockImpl implements MessageDao{

  @Override
  public boolean saveMessageFacts(AMQMessage message, String patientI2B2Id,
                                  Map<String, ODMMapping> conceptMappings) {
    return true;
  }

  @Override
  public boolean saveMessagePatient(AMQMessage message, String patientI2B2Id) {
    return true;
  }
}
