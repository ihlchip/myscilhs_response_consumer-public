/**
 * © 2014, Boston Children's Hospital. All Rights Reserved.
 * Developed by the Intelligent Health Lab at Children’s Hospital Informatics Program.
 * For more information, see http://chip.org/ihlab and https://github.com/chb
 */
package org.bch.myscilhs.dao;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.InputStreamRequestEntity;
import org.apache.commons.httpclient.methods.PostMethod;
import org.bch.myscilhs.pojo.AMQMessage;
import org.bch.myscilhs.pojo.ODMMapping;
import org.bch.myscilhs.soap.SendSoapFileUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * Saves the message facts to i2b2
 */
public class MessageDaoI2B2Impl implements MessageDao {

  private static final String SCHEMA_DEF = "<repository:patient_data xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n"
                                           + "   xmlns:repository=\"http://i2b2.mgh.harvard.edu/repository_cell\" xsi:schemaLocation=\"http://i2b2.mgh.harvard.edu/repository_cell/patient_data.xsd\">\n";
  private static final String OBS_SET_HEADER = "<observation_set>\n";
  private static final String OBS_SET_FOOTER = "</observation_set>\n"
                                               + "</repository:patient_data>\n";
  private static final String OBS_HEADER = "<observation>\n";
  private static final String OBS_FOOTER = "</observation>\n";
  private static final String PATIENT_SET_HEADER = "<patient_set>\n";
  private static final String PATIENT_SET_FOOTER = "</patient_set>\n"
                                               + "</repository:patient_data>\n";
  private static final String PATIENT_FOOTER = "</patient>\n";
  private static final String PID_SET_HEADER = "<pid_set>\n";
  private static final String PID_HEADER = "<pid>\n";
  private static final String PID_FOOTER = "</pid>\n";
  private static final String PID_SET_FOOTER = "</pid_set>\n";
  private static final String EID_SET_HEADER = "<eid_set>\n";
  private static final String EVENT_SET_HEADER =  "<event_set>\n";
  private static final String EVENT_SET_FOOTER =  "</event_set>\n";
  private static final String EID_HEADER = "<eid>\n";
  private static final String EID_FOOTER = "</eid>\n";
  private static final String EID_SET_FOOTER = "</eid_set>\n";
  private static final DateFormat sdf = new SimpleDateFormat("YYYY-MM-dd:HH:mm:ss.SSSS");
  private static final DateFormat sdfFile = new SimpleDateFormat("YYYY-MM-dd_HH_mm_ss");
  private static final DateFormat sdfI2B2 = new SimpleDateFormat("YYYY-MM-dd'T'HH:mm:ss");
  private static Logger logger = LoggerFactory.getLogger(MessageDaoI2B2Impl.class);
  private final String fileSuffix;
  private static final String patientFileSuffix = "_patient.xml";

  private final String i2b2SendService;
  private final String i2b2PublishService;
  private final String i2b2Domain;
  private final String i2b2User;
  private final String i2b2Password;
  private String i2b2SourceSystem;
  private String i2b2SourceInstitution;
  private String i2b2FileLocation;
  private String i2b2ProjectId;

  private MessageDaoI2B2Impl() {
    i2b2PublishService = null;
    fileSuffix = null;
    i2b2SendService = null;
    i2b2Domain = null;
    i2b2User = null;
    i2b2Password = null;
    i2b2SourceSystem = null;
    i2b2SourceInstitution = null;
    i2b2FileLocation = null;
    i2b2ProjectId= null;
  }

  /**
   *
   * @param i2b2Host
   * @param sendService
   * @param publishService
   * @param i2b2Domain
   * @param i2b2User
   * @param i2b2Password
   */
  public MessageDaoI2B2Impl(String i2b2Host, String sendService, String publishService,
                            String i2b2Domain, String i2b2User, String i2b2Password,
                            String i2b2SourceSystem, String i2b2SourceInstitution,
                            String i2b2ProjectId, String i2b2FileLocation) {
    this.i2b2SendService = i2b2Host + sendService;
    this.i2b2PublishService = i2b2Host + publishService;
    this.i2b2Domain = i2b2Domain;
    this.i2b2User = i2b2User;
    this.i2b2Password = i2b2Password;
    this.fileSuffix = "_myscilhs_" + this.hashCode() + ".xml";
    this.i2b2SourceInstitution = i2b2SourceInstitution;
    this.i2b2SourceSystem = i2b2SourceSystem;
    this.i2b2FileLocation = i2b2FileLocation;
    this.i2b2ProjectId = i2b2ProjectId;
  }

  /**
   * Sends a post request with the given xml payload to the given service.
   *
   * @param requestXml payload
   * @return the xml response from i2b2 in String format
   */
  String sendRequest(String requestXml) throws Exception {
    HttpClient client = new HttpClient();
    PostMethod postMethod = new PostMethod(i2b2PublishService);

    postMethod.setRequestEntity(new InputStreamRequestEntity(
        new ByteArrayInputStream(requestXml.getBytes("UTF-8"))));

    postMethod.setRequestHeader("Content-type",
                                "text/xml; charset=ISO-8859-1");
    String responseXml = null;

    int statusCode = client.executeMethod(postMethod);
    if (logger.isDebugEnabled()) {
      logger.debug("Received response code '" + statusCode + "' for request: " + requestXml);
    }
    responseXml = postMethod.getResponseBodyAsString();

    postMethod.releaseConnection();
    return responseXml;
  }

  /**
   * Assumes the field names have been converted to i2b2 concepts when creating the fieldMap.
   *
   * @return generated PDO xml
   */
  String convertMessagetoPDOV1(AMQMessage message, String patientI2B2Id,
                                      Map<String, ODMMapping> conceptMappings) throws Exception {
    Date now = new Date();

    Map<String, String> fieldMap = message.getSurveyFieldMap();
    String obsDate = sdfI2B2.format(now);

    StringBuilder sb = new StringBuilder();
    sb.append(SCHEMA_DEF);
    // eid

    // pid
    sb.append(PID_SET_HEADER);
    sb.append(PID_HEADER);
    sb.append("<patient_id update_date=\"" + obsDate + "\"\n"
              + "sourcesystem_cd=\"" + i2b2SourceSystem + "\" source=\"" + i2b2SourceInstitution
              + "\">" + patientI2B2Id + "</patient_id>");
    sb.append(PID_FOOTER);
    sb.append(PID_SET_FOOTER);
    sb.append(EID_SET_HEADER);
    sb.append(EID_HEADER);
    sb.append(
        "<event_id source=\"" + i2b2SourceInstitution + "\" patient_id=\"" + patientI2B2Id + "\"\n"
        + "patient_id_source=\"" + i2b2SourceInstitution + "\"\n"
        + "update_date=\"" + obsDate + "\"\n"
        + "sourcesystem_cd=\"" + i2b2SourceSystem + "\">" + message.getEventId() + "</event_id>\n");
    sb.append(EID_FOOTER);
    sb.append(EID_SET_FOOTER);
    sb.append(EVENT_SET_HEADER);
    sb.append("<event update_date=\""+obsDate+"\"\n");
    sb.append("download_date=\""+obsDate+"\"\n");
    sb.append("import_date=\""+obsDate+"\"\n");
    sb.append("sourcesystem_cd=\""+i2b2SourceSystem+"\">\n");
    sb.append("<event_id source=\""+i2b2SourceInstitution+"\">2</event_id>\n");
    sb.append("<patient_id source=\""+i2b2SourceInstitution+"\">"+patientI2B2Id+"</patient_id>\n");
    sb.append("<start_date>"+obsDate+"</start_date>\n");
    sb.append("<end_date>"+obsDate+"</end_date>\n");
    sb.append("</event>\n");
    sb.append(EVENT_SET_FOOTER);
    sb.append(OBS_SET_HEADER);
    for (String fieldKey : fieldMap.keySet()) {
      ODMMapping fieldMapping = conceptMappings.get(fieldKey);

      if (fieldMapping != null) {
        String valType =
            (fieldMapping.getType() == null || fieldMapping.getType().equals("text")) ? "T" : "N";

        sb.append(OBS_HEADER);
        sb.append("<event_id source=\"" + i2b2SourceInstitution + "\">" + message.getEventId()
                  + "</event_id>\n"); // Unique encounter id is generated in i2b2 from event + source.
        sb.append("<patient_id source=\""+i2b2SourceInstitution+"\">" + patientI2B2Id + "</patient_id>\n");
        sb.append("<concept_cd>" + fieldMapping.getConceptCode() + "</concept_cd>\n");
        sb.append("<observer_cd>" + i2b2SourceInstitution + "</observer_cd>\n");

        //TODO: Get timestamp from message and load here
        sb.append("<start_date>" + obsDate + "</start_date>\n");
        sb.append("<instance_num>1</instance_num>\n");
        sb.append("<modifier_cd>@</modifier_cd>\n"); // @==Null in i2b2. Recommended for consistency
        sb.append("<valuetype_cd>" + valType + "</valuetype_cd>\n");
        if (valType.equals("N")) {
          sb.append("<nval_num>" + fieldMap.get(fieldKey) + "</nval_num>\n");
        } else {
          sb.append("<tval_char>" + fieldMap.get(fieldKey) + "</tval_char>\n");
        }
        sb.append("<end_date>" + obsDate + "</end_date>\n");
        sb.append("<location_cd>" + i2b2SourceInstitution + "</location_cd>\n");

        sb.append(OBS_FOOTER);
      } else {
        if (logger.isWarnEnabled()) {
          logger.warn("Dropping field: \'" + fieldKey + "\'. Not found in concept table!");
        }
      }
    }
    sb.append(OBS_SET_FOOTER);

    return sb.toString();
  }

  /**
   * Assumes the field names have been converted to i2b2 concepts when creating the fieldMap.
   *
   * @return generated PDO xml
   */
  String createPatientPDO(String patientI2B2Id) throws Exception {
    Date now = new Date();
    //TODO:  Use timestamp from message instead
    String obsDate = sdfI2B2.format(now);

    StringBuilder sb = new StringBuilder();
    sb.append(SCHEMA_DEF);

    sb.append(PATIENT_SET_HEADER);
    sb.append("<patient update_date=\"" + obsDate + "\" sourcesystem_cd=\"" + i2b2SourceSystem  + "\">");
    sb.append("<patient_id source=\"" + i2b2SourceInstitution + "\">"+patientI2B2Id+"</patient_id>\n");

    sb.append(PATIENT_FOOTER);
    sb.append(PATIENT_SET_FOOTER);

    return sb.toString();
  }

  @Override
  public boolean saveMessagePatient(AMQMessage message, String patientI2B2Id) {

    logger.debug("Saving message: " + message.getSubjectId());

    boolean result = false;
    // Construct the PDO
    String pdo = null;
    try {
      pdo = this.createPatientPDO(patientI2B2Id);
    } catch (Exception e) {
      if (logger.isErrorEnabled()) {
        logger.error("Could not convert message to PDO!", e);
      }
    }

    if (pdo != null && !pdo.isEmpty()) {
      String date = sdfI2B2.format(new Date());
      String fileName = this.generatePatientFileName(message);
      String fileSize = SendSoapFileUtil.calcFileSize(pdo);
      String fileHash = SendSoapFileUtil.calcFileHash(pdo);
      String fileRequest = SendSoapFileUtil.generateFileSendRequest(date, i2b2Domain, i2b2User,
                                                                    i2b2Password,
                                                                    i2b2ProjectId, fileSize,
                                                                    fileName, fileHash);

      logger.debug("Using Service: " + i2b2SendService);
      // Send file to i2b2
      try {
        String response = SendSoapFileUtil
            .sendSOAP(i2b2SendService, fileRequest, fileName, pdo);
        if(logger.isDebugEnabled()) {
          logger.debug("SOAP Response: "+response);
        }

        if (!response.contains("Error") && !response.contains("Exception")) {
          result = true;
        }
        if(result){
          logger.debug("Using Service: " + i2b2PublishService);
          String saveRequest = SendSoapFileUtil.generateFileSaveRequest(date, i2b2Domain, i2b2User,
                                                                        i2b2Password,
                                                                        i2b2ProjectId,
                                                                        i2b2FileLocation + i2b2ProjectId + "/" + fileName,
                                                                        fileName);
          String restResponse = this.sendRequest(saveRequest);
          if(logger.isDebugEnabled()) {
            logger.debug("REST Response: "+restResponse);
          }
          if (response.contains("Error") || response.contains("Exception")) {
            result = false;
          }
        }
      } catch (Throwable t) {
        if (logger.isErrorEnabled()) {
          logger.error("Unable to send pdo file to i2b2!", t);
        }
      }
    }
    return result;
  }

  @Override
  public boolean saveMessageFacts(AMQMessage message, String patientI2B2Id,
                                  Map<String, ODMMapping> conceptMappings) {

    logger.debug("Saving message: " + message.getSubjectId());

    boolean result = false;
    // Construct the PDO
    String pdo = null;
    try {
      pdo = this.convertMessagetoPDO(message, patientI2B2Id, conceptMappings);
    } catch (Exception e) {
      if (logger.isErrorEnabled()) {
        logger.error("Could not convert message to PDO!", e);
      }
    }

    if (pdo != null && !pdo.isEmpty()) {
      String date = sdfI2B2.format(new Date());
      String fileName = this.generateFileName(message);
      String fileSize = SendSoapFileUtil.calcFileSize(pdo);
      String fileHash = SendSoapFileUtil.calcFileHash(pdo);
      String fileRequest = SendSoapFileUtil.generateFileSendRequest(date, i2b2Domain, i2b2User,
                                                                    i2b2Password,
                                                                    i2b2ProjectId, fileSize,
                                                                    fileName, fileHash);

      logger.debug("Using Service: " + i2b2SendService);
      // Send file to i2b2
      try {
        String response = SendSoapFileUtil
            .sendSOAP(i2b2SendService, fileRequest, fileName, pdo);
        if(logger.isDebugEnabled()) {
          logger.debug("SOAP Response: "+response);
        }

        if (!response.contains("ERROR") && !response.contains("Exception")) {
          result = true;
        }
        if(result){
          logger.debug("Using Service: " + i2b2PublishService);
          String saveRequest = SendSoapFileUtil.generateFileSaveRequest(date, i2b2Domain, i2b2User,
                                                                        i2b2Password,
                                                                        i2b2ProjectId,
                                                                        i2b2FileLocation + i2b2ProjectId + "/" + fileName,
                                                                        fileName);
          String restResponse = this.sendRequest(saveRequest);
          if(logger.isDebugEnabled()) {
            logger.debug("REST Response: "+restResponse);
          }
          if (response.contains("ERROR") || response.contains("Exception")) {
            result = false;
          }
        }
      } catch (Throwable t) {
        if (logger.isErrorEnabled()) {
          logger.error("Unable to send pdo file to i2b2!", t);
        }
      }
    }
    return result;
  }

  public String convertMessagetoPDO(AMQMessage message, String patientI2B2Id,
                                    Map<String, ODMMapping> conceptMappings) throws Exception {

    return this.convertMessagetoPDOV1(message, patientI2B2Id, conceptMappings);
  }

  String generateFileName(AMQMessage message) {
    Date now = new Date();
    return sdfFile.format(now) + "_" + message.getSubjectId() + fileSuffix;
  }

  String generatePatientFileName(AMQMessage message) {
    Date now = new Date();
    return sdfFile.format(now) + "_" + message.getSubjectId() + patientFileSuffix;
  }
}
