/**
 * © 2014, Boston Children's Hospital. All Rights Reserved.
 * Developed by the Intelligent Health Lab at Children’s Hospital Informatics Program.
 * For more information, see http://chip.org/ihlab and https://github.com/chb
 */
package org.bch.myscilhs.dao;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthPolicy;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.GetMethod;
import org.bch.myscilhs.pojo.AMQMessageVoxeo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This will be handled by a CSV Uploader service. For test purposes I will load from a file.
 */
public class I2B2IdDaoRestImpl implements I2B2IdDao {

  private final Logger logger = LoggerFactory.getLogger(I2B2IdDaoRestImpl.class);
  private final String restProtocol;
  private final String restHost;
  private final String restPort;
  private final String restURL;
  private final String username;
  private final String password;

  private I2B2IdDaoRestImpl() {
    restProtocol=null; restHost=null; restPort=null; restURL=null; username=null; password=null;
  }

  public I2B2IdDaoRestImpl(String restProtocol, String restHost, String restPort, String restURL,
                           String username, String password) {

    this.restProtocol = restProtocol;
    this.restHost = restHost;
    this.restPort = restPort;
    this.restURL = restURL;
    this.username = username;
    this.password = password;
  }

  /**
   * This should call a CSV upload service, but for first pass its just reads a file at the
   * specified location.
   */
  public Map<String, String> fetchi2b2Identifiers(List<AMQMessageVoxeo> messages) {
    Map<String, String> messagePublicToi2b2Map = new HashMap<>(messages.size());

    for (AMQMessageVoxeo message : messages) {
      String publicId = message.getSubjectId();
      String i2b2Id = callRest(publicId);

      if (i2b2Id != null) {
        messagePublicToi2b2Map.put(publicId, i2b2Id);
      } else {
        if (logger.isErrorEnabled()) {
          logger.error("Could not find i2b2 id for record id: " + message.getRecordId()
                       + " public id: " + publicId);
        }
      }
    }

    return messagePublicToi2b2Map;
  }

  String callRest(String publicId){
    List<String> authPrefs = new ArrayList<String>(2);
    authPrefs.add(AuthPolicy.DIGEST);
    authPrefs.add(AuthPolicy.BASIC);
    HttpClient client = new HttpClient();
    client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);
    client.getParams().setAuthenticationPreemptive(true);
    client.getState().setCredentials(new AuthScope(restHost, AuthScope.ANY_PORT,
                                                   AuthScope.ANY_REALM),
                                     new UsernamePasswordCredentials(username, password));


    String restEndpoint = restProtocol+restHost+":"+restPort+restURL;
    GetMethod getMethod = new GetMethod(restEndpoint + "?publicId="+publicId);
    getMethod.setDoAuthentication(true);
    getMethod.setFollowRedirects(false);

    String response = null;

    try {
      int statusCode = client.executeMethod(getMethod);
      if (logger.isDebugEnabled()) {
        logger.debug("Received response code '" + statusCode + "' for request: " + getMethod.getName());
      }
      String responseBody = getMethod.getResponseBodyAsString();
      if(statusCode == 200 && !responseBody.contains("login")) {
        response = responseBody;
      } else {
        logger.debug("Received bad response from public id service REST call responseCode: "+statusCode+" response: "+responseBody);
      }
    }catch (Exception e){
      if(logger.isErrorEnabled()){
        logger.error("Rest Call to public id service failed!", e);
      }
    }

    getMethod.releaseConnection();
    return response;
  }
}
