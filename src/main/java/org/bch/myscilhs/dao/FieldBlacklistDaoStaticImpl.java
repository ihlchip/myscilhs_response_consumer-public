/**
 * © 2014, Boston Children's Hospital. All Rights Reserved.
 * Developed by the Intelligent Health Lab at Children’s Hospital Informatics Program.
 * For more information, see http://chip.org/ihlab and https://github.com/chb
 */
package org.bch.myscilhs.dao;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by paul on 12/5/14.
 */
public class FieldBlacklistDaoStaticImpl implements FieldBlacklistDao {
  static final List<String> BLACK_LISTED_FIELDS = new ArrayList<>();
  static {
    BLACK_LISTED_FIELDS.add("ped_f_name");
    BLACK_LISTED_FIELDS.add("ped_l_name");
    BLACK_LISTED_FIELDS.add("ped_home_phone");
    BLACK_LISTED_FIELDS.add("ped_mobile_phone");
    BLACK_LISTED_FIELDS.add("ped_email");
    BLACK_LISTED_FIELDS.add("ped_st_add1");
    BLACK_LISTED_FIELDS.add("ped_city");
    BLACK_LISTED_FIELDS.add("ped_state");
    BLACK_LISTED_FIELDS.add("ped_zip");
    BLACK_LISTED_FIELDS.add("ped_mid_name");
    BLACK_LISTED_FIELDS.add("ped_st_add2");
    BLACK_LISTED_FIELDS.add("intro_text");
    BLACK_LISTED_FIELDS.add("callback");
    BLACK_LISTED_FIELDS.add("answ_machine_response");
    BLACK_LISTED_FIELDS.add("recieve_optout");
    BLACK_LISTED_FIELDS.add("resendletter");
    BLACK_LISTED_FIELDS.add("consent");
    BLACK_LISTED_FIELDS.add("anyquestions");
    BLACK_LISTED_FIELDS.add("parent_f_name");
    BLACK_LISTED_FIELDS.add("res_oth_comm");
    BLACK_LISTED_FIELDS.add("transfer_text");
    BLACK_LISTED_FIELDS.add("f_name");
    BLACK_LISTED_FIELDS.add("l_name");
    BLACK_LISTED_FIELDS.add("mid_name");
    BLACK_LISTED_FIELDS.add("home_phone");
    BLACK_LISTED_FIELDS.add("email");
    BLACK_LISTED_FIELDS.add("st_add1");
    BLACK_LISTED_FIELDS.add("st_add2");
    BLACK_LISTED_FIELDS.add("state");
    BLACK_LISTED_FIELDS.add("city");
    BLACK_LISTED_FIELDS.add("zip");
    BLACK_LISTED_FIELDS.add("nopickup_date");
    BLACK_LISTED_FIELDS.add("speak_part_othcom");
    BLACK_LISTED_FIELDS.add("confirm_number");
    BLACK_LISTED_FIELDS.add("dob_nomatch_explain");
    BLACK_LISTED_FIELDS.add("read_letter");
    BLACK_LISTED_FIELDS.add("administerconsent");
    BLACK_LISTED_FIELDS.add("no_participate");
    BLACK_LISTED_FIELDS.add("contact_wrong");
    BLACK_LISTED_FIELDS.add("parent_l_name");
    BLACK_LISTED_FIELDS.add("finish_live_int");
  }

  @Override
  public List<String> fetchBlacklistedFields() {
    return BLACK_LISTED_FIELDS;
  }
}
