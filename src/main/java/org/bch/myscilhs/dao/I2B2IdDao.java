/**
 * © 2014, Boston Children's Hospital. All Rights Reserved.
 * Developed by the Intelligent Health Lab at Children’s Hospital Informatics Program.
 * For more information, see http://chip.org/ihlab and https://github.com/chb
 */
package org.bch.myscilhs.dao;

import org.bch.myscilhs.pojo.AMQMessageVoxeo;

import java.util.List;
import java.util.Map;

public interface I2B2IdDao {

  public Map<String, String> fetchi2b2Identifiers(List<AMQMessageVoxeo> messages);
}
