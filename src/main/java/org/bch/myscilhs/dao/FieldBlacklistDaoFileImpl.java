/**
 * © 2014, Boston Children's Hospital. All Rights Reserved.
 * Developed by the Intelligent Health Lab at Children’s Hospital Informatics Program.
 * For more information, see http://chip.org/ihlab and https://github.com/chb
 */
package org.bch.myscilhs.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.List;

/**
 * Loads blacklist from specified file location.
 */
public class FieldBlacklistDaoFileImpl implements FieldBlacklistDao{

  private final Logger logger = LoggerFactory.getLogger(FieldBlacklistDaoFileImpl.class);
  private final String fileLocation;

  private FieldBlacklistDaoFileImpl() {
    fileLocation = null;
  }

  public FieldBlacklistDaoFileImpl(String fileLocation) {
    this.fileLocation = fileLocation;
  }

  @Override
  public List<String> fetchBlacklistedFields() {
    throw new NotImplementedException();
    //return null;
  }
}
