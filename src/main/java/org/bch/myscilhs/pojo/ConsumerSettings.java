/**
 * © 2014, Boston Children's Hospital. All Rights Reserved.
 * Developed by the Intelligent Health Lab at Children’s Hospital Informatics Program.
 * For more information, see http://chip.org/ihlab and https://github.com/chb
 */
package org.bch.myscilhs.pojo;

import java.io.File;

public class ConsumerSettings {

  public final String MQ_QUEUE_NAME;
  public final String MQ_PROTOCOL;
  public final String MQ_HOST;
  public final String MQ_PORT;
  public final String MQ_USER;

  public final String MQ_PASSWORD;
  public final String MQ_KEYSTORE;
  public final String MQ_TRUSTSTORE;
  public final String MQ_KEYSTORE_PASSWORD;

  public final String I2B2_DOMAIN;
  public final String I2B2_USER;
  public final String I2B2_PASSWORD;
  public final String I2B2_HOST;
  public final String I2B2_SOURCE_INSTITUTION;
  public final String I2B2_SOURCE_SYSTEM;
  public final String I2B2_FILE_LOCATION;
  public final String I2B2_PROJECT_ID;

  public final String FILE_PUBLISH_SERVICE;
  public final String FILE_SEND_SERVICE;

  public final String CONCEPT_FILES_LOCATION;

  public final String PUBLIC_ID_PROTOCOL;
  public final String PUBLIC_ID_HOST;
  public final String PUBLIC_ID_PORT;
  public final String PUBLIC_ID_URL;
  public final String PUBLIC_ID_USER;
  public final String PUBLIC_ID_PASS;

  public ConsumerSettings(String MQ_QUEUE_NAME, String MQ_PROTOCOL, String MQ_HOST,
                          String MQ_PORT, String MQ_USER, String MQ_PASSWORD,
                          String MQ_KEYSTORE, String MQ_TRUSTSTORE,
                          String MQ_KEYSTORE_PASSWORD, String i2B2_DOMAIN, String i2B2_USER,
                          String i2B2_PASSWORD, String i2B2_HOST, String I2B2_SOURCE_INSTITUTION,
                          String I2B2_SOURCE_SYSTEM, String I2B2_PROJECT_ID,
                          String I2B2_FILE_LOCATION, String FILE_PUBLISH_SERVICE, String FILE_SEND_SERVICE,
                          String CONCEPT_FILES_LOCATION, String PUBLIC_ID_PROTOCOL,
                          String PUBLIC_ID_HOST, String PUBLIC_ID_PORT, String PUBLIC_ID_URL,
                          String PUBLIC_ID_USER, String PUBLIC_ID_PASS) {
    this.MQ_QUEUE_NAME = MQ_QUEUE_NAME;
    this.MQ_PROTOCOL = MQ_PROTOCOL;
    this.MQ_HOST = MQ_HOST;
    this.MQ_PORT = MQ_PORT;
    this.MQ_USER = MQ_USER;
    this.MQ_PASSWORD = MQ_PASSWORD;
    this.MQ_KEYSTORE = new File(MQ_KEYSTORE).toURI().toString();
    this.MQ_TRUSTSTORE = new File(MQ_TRUSTSTORE).toURI().toString();
    this.MQ_KEYSTORE_PASSWORD = MQ_KEYSTORE_PASSWORD;
    this.I2B2_DOMAIN = i2B2_DOMAIN;
    this.I2B2_USER = i2B2_USER;
    this.I2B2_PASSWORD = i2B2_PASSWORD;
    this.I2B2_HOST = i2B2_HOST;
    this.I2B2_SOURCE_INSTITUTION = I2B2_SOURCE_INSTITUTION;
    this.I2B2_SOURCE_SYSTEM = I2B2_SOURCE_SYSTEM;
    this.I2B2_FILE_LOCATION = I2B2_FILE_LOCATION;
    this.I2B2_PROJECT_ID = I2B2_PROJECT_ID;
    this.FILE_PUBLISH_SERVICE = FILE_PUBLISH_SERVICE;
    this.FILE_SEND_SERVICE = FILE_SEND_SERVICE;
    this.CONCEPT_FILES_LOCATION = CONCEPT_FILES_LOCATION;
    this.PUBLIC_ID_PROTOCOL = PUBLIC_ID_PROTOCOL;
    this.PUBLIC_ID_HOST = PUBLIC_ID_HOST;
    this.PUBLIC_ID_PORT = PUBLIC_ID_PORT;
    this.PUBLIC_ID_URL = PUBLIC_ID_URL;
    this.PUBLIC_ID_USER = PUBLIC_ID_USER;
    this.PUBLIC_ID_PASS = PUBLIC_ID_PASS;
  }
}
