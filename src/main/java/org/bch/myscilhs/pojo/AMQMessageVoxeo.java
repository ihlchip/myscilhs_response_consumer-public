/**
 * © 2014, Boston Children's Hospital. All Rights Reserved.
 * Developed by the Intelligent Health Lab at Children’s Hospital Informatics Program.
 * For more information, see http://chip.org/ihlab and https://github.com/chb
 */
package org.bch.myscilhs.pojo;

/**
 * Holds the metadata required for a survey response. The survey field map is a map of question keys
 * to answer values.This class guarantees the survey field map and the question keys and the answer
 * values will never be null.
 */
public class AMQMessageVoxeo extends AMQMessage {

  /**
   * The field surveyFieldMap is initialized as a {@link java.util.LinkedHashMap} to preserve
   * order.
   *
   * @param recordId  Identifies a unique response.
   * @param subjectId Unique id mapped to this subject's i2b2 id internally.
   * @param surveyId  The survey identifier.
   * @param versionId Version of the survey used.
   * @param eventId   Event for this survey and user. It is analogous to a session id.
   * @param armId     Survey arm of this response.
   */
  public AMQMessageVoxeo(String recordId, String subjectId, String surveyId, String versionId,
                         String eventId, String armId) {
    super(recordId, subjectId, surveyId, versionId, eventId, armId);
  }

}
