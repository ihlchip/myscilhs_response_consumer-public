/**
 * © 2014, Boston Children's Hospital. All Rights Reserved.
 * Developed by the Intelligent Health Lab at Children’s Hospital Informatics Program.
 * For more information, see http://chip.org/ihlab and https://github.com/chb
 */
package org.bch.myscilhs.pojo;

/**
 * Holds the required information for mapping a response field to a concept code, varName, and
 * answer as part of an i2b2 PDO.
 */
public class ODMMapping {

  private final String odmCode;
  private final String conceptCode;
  private final String varName;
  private final String type;
  private final String answer;

  private ODMMapping() {
    odmCode = null;
    conceptCode = null;
    varName = null;
    type = null;
    answer = null;
  }

  public ODMMapping(String odmCode, String conceptCode, String varName, String type,
                    String answer) {
    this.odmCode = odmCode;
    this.conceptCode = conceptCode;
    this.varName = varName;
    this.type = type;
    this.answer = answer;
  }

  public String getOdmCode() {
    return odmCode;
  }

  public String getConceptCode() {
    return conceptCode;
  }

  public String getVarName() {
    return varName;
  }

  public String getAnswer() {
    return answer;
  }

  public String getType() {
    return type;
  }

  @Override
  public String toString() {
    return "ODMMapping{" +
           "odmCode='" + odmCode + '\'' +
           ", conceptCode='" + conceptCode + '\'' +
           ", varName='" + varName + '\'' +
           ", type='" + type + '\'' +
           ", answer='" + answer + '\'' +
           '}';
  }
}
