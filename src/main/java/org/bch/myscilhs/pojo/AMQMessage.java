/**
 * © 2014, Boston Children's Hospital. All Rights Reserved.
 * Developed by the Intelligent Health Lab at Children’s Hospital Informatics Program.
 * For more information, see http://chip.org/ihlab and https://github.com/chb
 */
package org.bch.myscilhs.pojo;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Holds the metadata required for a survey response. The survey field map is a map of question keys
 * to answer values.This class guarantees the survey field map and the question keys and the answer
 * values will never be null.
 */
public class AMQMessage {

  public static final String DEFAULT_ANSWER_VALUE = "no answer";

  private final String recordId;
  private final String subjectId;
  private final String surveyId;
  private final String versionId;
  private final String eventId;
  private final String armId;
  private final Map<String, String> surveyFieldMap;

  /**
   * Private default constructor to prevent use and preserve contracts.
   */
  @SuppressWarnings("unused")
  private AMQMessage() {
    this.recordId = null;
    this.subjectId = null;
    this.surveyId = null;
    this.versionId = null;
    this.eventId = null;
    this.armId = null;
    this.surveyFieldMap = null;
  }

  /**
   * The field surveyFieldMap is initialized as a {@link java.util.LinkedHashMap} to preserve
   * order.
   *
   * @param recordId  Identifies a unique response.
   * @param subjectId Unique id mapped to this subject's i2b2 id internally.
   * @param surveyId  The survey identifier.
   * @param versionId Version of the survey used.
   * @param eventId   Event for this survey and user. It is analogous to a session id.
   * @param armId     Survey arm of this response.
   */
  public AMQMessage(String recordId, String subjectId, String surveyId, String versionId,
                    String eventId, String armId) {
    this.recordId = recordId;
    this.subjectId = subjectId;
    this.surveyId = surveyId;
    this.versionId = versionId;
    this.eventId = eventId;
    this.armId = armId;
    this.surveyFieldMap = new LinkedHashMap<>();
  }

  /**
   * Adds the given key and value pair to the end of linked survey field map. If the answer value is
   * null it is replaced with {@value #DEFAULT_ANSWER_VALUE}
   *
   * @param questionKey A key which identifies the question in the survey. Derived from RedCap.
   * @param answerValue Answer provided by the subject
   * @throws IllegalArgumentException when the questionKey is null
   */
  public void addSurveyField(String questionKey, String answerValue)
      throws IllegalArgumentException {

    if (questionKey == null) {
      throw new IllegalArgumentException("The questionKey cannot be null.");
    }

    String value = answerValue;
    if (value == null) {
      value = DEFAULT_ANSWER_VALUE;
    }

    surveyFieldMap.put(questionKey, value);
  }

  /**
   * Returns a shallow copy to preserve immutability except through the accessor method. The Map
   * implementation used is {@link java.util.LinkedHashMap}.
   *
   * @return The survey field map
   */
  public Map<String, String> getSurveyFieldMap() {
    return new LinkedHashMap<>(surveyFieldMap);
  }

  public String getRecordId() {
    return recordId;
  }

  public String getSubjectId() {
    return subjectId;
  }

  public String getSurveyId() {
    return surveyId;
  }

  public String getVersionId() {
    return versionId;
  }

  public String getEventId() {
    return eventId;
  }

  public String getArmId() {
    return armId;
  }

  @Override
  public String toString() {
    return "AMQMessageVoxeo{" +
           "recordId='" + recordId + '\'' +
           ", subjectId='" + subjectId + '\'' +
           ", surveyId='" + surveyId + '\'' +
           ", versionId='" + versionId + '\'' +
           ", eventId='" + eventId + '\'' +
           ", armId='" + armId + '\'' +
           ", surveyFieldMap=" + surveyFieldMap +
           '}';
  }
}
